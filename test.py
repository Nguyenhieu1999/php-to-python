import pickle

#doc_new = ['obama is running for president in 2016']

var = input("Nhập đoạn văn bản cần xác thực: ")
print("Văn bản đã nhập " + str(var))

#function to run for prediction
def detecting_fake_news(var):
    load_model = pickle.load(open('final_model.sav', 'rb'))
    prediction = load_model.predict([var])
    prob = load_model.predict_proba([var])

    return (print("Kết quả dự đoán: ",prediction[0]),
    print("Điểm tin cậy: ",prob[0][1]))


if __name__ == '__main__':
    detecting_fake_news(var)
