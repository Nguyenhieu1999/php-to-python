import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from Train import TrainBase
from DataPreProcess import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier

class Training_MLPClassifier(TrainBase):
    #OVERRIDE
    def train_to_model(self, ratio=0.8, hidden_layer_sizes=(100, ), activation='relu', *, solver='adam', alpha=0.0001, batch_size='auto', learning_rate='constant', 
        learning_rate_init=0.001, power_t=0.5, max_iter=200, shuffle=True, random_state=None, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, 
        nesterovs_momentum=True, early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08, n_iter_no_change=10, max_fun=15000):

        self.ratio = ratio
        
        train_data, test_data, train_labels, test_labels = train_test_split(self.vectors, self.labels, int(self.size_of_data * ratio))

        model = MLPClassifier(
            hidden_layer_sizes=hidden_layer_sizes, activation=activation, solver=solver, alpha=alpha, batch_size=batch_size,learning_rate=learning_rate,  learning_rate_init=learning_rate_init, 
            power_t=power_t, max_iter=max_iter, shuffle=shuffle, random_state=random_state, tol=tol, verbose=verbose, warm_start=warm_start, momentum=momentum,nesterovs_momentum=nesterovs_momentum, 
            early_stopping=early_stopping, validation_fraction=validation_fraction, beta_1=beta_1, beta_2=beta_2, epsilon=epsilon, n_iter_no_change=n_iter_no_change, max_fun=max_fun
        )

        model.fit(train_data, train_labels)

        self.accuracy = accuracy_score(model.predict(test_data), test_labels)
        self.model = model