import sys
import os
from joblib import dump, load
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from DataPreProcess import DataProcess
import numpy as np
import pandas as pd

class TrainBase:
    def __init__(self, data_name, sep=","):
        if os.name == 'nt':
            # self.data = np.array(pd.read_csv("D:/AE_Final/Train/" + data_name, sep=sep, header=None))
            pass
        else:
            try:
                self.data = np.array(pd.read_csv(data_name, sep=sep))
            except FileNotFoundError:
                self.data = np.array(pd.read_csv(constant.toTrainpath() + data_name, sep=sep))
        
        self.size_of_data = self.data.shape[0]
        
        self.first_products = self.data[:, 0]
        self.second_products = self.data[:, 1]
        self.labels = np.array(self.data[:, 2], dtype=int)
    
    def process_data(self):
        dataProcess = DataProcess(self.first_products, self.second_products, self.labels)
        self.vectors, self.labels = dataProcess.process()
    
    def train_to_model(self):
        self.accuracy = 0
        self.ratio = 0
        self.model = object()

    def print_acc(self):
        return '''Accuracy score: {:2f}%'''.format(self.accuracy * 100)

    def print_and_save(self, model_name):
        '''Example for model name: SGDClassifier.joblib'''
        if os.name == 'nt':
            # dump(self.model, "D:/AE_Final/Model/" + model_name)
            pass
        else:
            dump(self.model, constant.toModelpath() + model_name)
        return 'Done!'
