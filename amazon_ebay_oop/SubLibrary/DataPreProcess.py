from StringProcess import StringProcess

stringProcess = StringProcess()

class DataProcess:
    def __init__(self, first_products, second_products, labels):
        '''Initialize an instance'''
        self.number_of_data = first_products.shape[0]
        self.first_products = first_products
        self.second_products = second_products
        self.labels = labels
        self.vectors = []
    
    def process(self):
        '''Process data'''
        for number in range(self.number_of_data):
            s1 = self.first_products[number]
            s2 = self.second_products[number]
            self.vectors.append(stringProcess.string_vectorizer(s1, s2))
        
        return (self.vectors, self.labels)


def train_test_split(vectors, labels, number_of_train=10):
    return (vectors[:number_of_train], vectors[number_of_train:], labels[:number_of_train], labels[number_of_train:])


